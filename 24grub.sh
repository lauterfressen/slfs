grub-install --target i386-pc /dev/vda

cat > /boot/grub/grub.cfg << "EOF"
# Begin /boot/grub/grub.cfg
set default=0
set timeout=5

insmod ext2
set root=(hd0,2)

menuentry "GNU/Linux, Linux 5.10.17-lfs-10.1" {
        linux   /boot/vmlinuz-5.10.17-lfs-10.1 root=/dev/vda2 ro
}
EOF 
