#GMP-6.2.1

echo GMP-6.2.1 >> err
tar -xf gmp-6.2.1.tar.*
cd gmp-6.2.1
./configure --prefix=/usr    \
            --enable-cxx     \
            --disable-static \
            --docdir=/usr/share/doc/gmp-6.2.1 \
            --build=x86_64-unknown-linux-gnu
make
make html
make check 2>&1 | tee /sources/gmp-check-log
awk '/# PASS:/{total+=$3} ; END{print total}' /sources/gmp-check-log
make install
make install-html

cd $LFS/sources
rm -rf gmp-6.2.1