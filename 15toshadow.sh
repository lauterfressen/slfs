#MPC-1.2.1

echo MPC-1.2.1 >> err
tar -xf mpc-1.2.1.tar.*
cd mpc-1.2.1
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/mpc-1.2.1
make
make html

make install
make install-html
cd $LFS/sources
rm -rf mpc-1.2.1

#Attr-2.4.48

echo Attr-2.4.48 >> err
tar -xf attr-2.4.48.tar.*
cd attr-2.4.48
./configure --prefix=/usr     \
            --bindir=/bin     \
            --disable-static  \
            --sysconfdir=/etc \
            --docdir=/usr/share/doc/attr-2.4.48
make

make install
mv -v /usr/lib/libattr.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libattr.so) /usr/lib/libattr.so
cd $LFS/sources
rm -rf attr-2.4.48

#Acl-2.2.53

echo Acl-2.2.53 >> err
tar -xf acl-2.2.53.tar.*
cd acl-2.2.53
./configure --prefix=/usr         \
            --bindir=/bin         \
            --disable-static      \
            --libexecdir=/usr/lib \
            --docdir=/usr/share/doc/acl-2.2.53
make
make install
mv -v /usr/lib/libacl.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libacl.so) /usr/lib/libacl.so
cd $LFS/sources
rm -rf acl-2.2.53

#Libcap-2.48

echo Libcap-2.48 >> err
tar -xf libcap-2.48.tar.*
cd libcap-2.48
sed -i '/install -m.*STA/d' libcap/Makefile
make prefix=/usr lib=lib

make prefix=/usr lib=lib install
for libname in cap psx; do
    mv -v /usr/lib/lib${libname}.so.* /lib
    ln -sfv ../../lib/lib${libname}.so.2 /usr/lib/lib${libname}.so
    chmod -v 755 /lib/lib${libname}.so.2.48
done
cd $LFS/sources
rm -rf libcap-2.48

#Shadow-4.8.1

echo Shadow-4.8.1 >> err
tar -xf shadow-4.8.1.tar.*
cd shadow-4.8.1
sed -i 's/groups$(EXEEXT) //' src/Makefile.in
find man -name Makefile.in -exec sed -i 's/groups\.1 / /'   {} \;
find man -name Makefile.in -exec sed -i 's/getspnam\.3 / /' {} \;
find man -name Makefile.in -exec sed -i 's/passwd\.5 / /'   {} \;
sed -e 's:#ENCRYPT_METHOD DES:ENCRYPT_METHOD SHA512:' \
    -e 's:/var/spool/mail:/var/mail:'                 \
    -i etc/login.defs

sed -i 's/1000/999/' etc/useradd
touch /usr/bin/passwd
./configure --sysconfdir=/etc \
            --with-group-name-max-length=32
make
make install
cd $LFS/sources
rm -rf shadow-4.8.1 

pwconv
grpconv
passwd root 
