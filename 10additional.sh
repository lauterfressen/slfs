
#GCC-10.2.0

echo GCC-10.2.0 >> err
tar -xf gcc-10.2.0.tar.*
cd gcc-10.2.0
ln -s gthr-posix.h libgcc/gthr-default.h
mkdir -v build
cd       build
../libstdc++-v3/configure            \
    CXXFLAGS="-g -O2 -D_GNU_SOURCE"  \
    --prefix=/usr                    \
    --disable-multilib               \
    --disable-nls                    \
    --host=$(uname -m)-lfs-linux-gnu \
    --disable-libstdcxx-pch
make
make install
cd $LFS/sources
rm -rf gcc-10.2.0

#Gettext-0.21

echo Gettext-0.21 >> err
tar -xf gettext-0.21.tar.*
cd gettext-0.21
./configure --disable-shared
make
cp -v gettext-tools/src/{msgfmt,msgmerge,xgettext} /usr/bin
cd $LFS/sources
rm -rf gettext-0.21

#Bison-3.7.5

echo Bison-3.7.5 >> err
tar -xf bison-3.7.5.tar.*
cd bison-3.7.5
./configure --prefix=/usr \
            --docdir=/usr/share/doc/bison-3.7.5
make
make install
cd $LFS/sources
rm -rf bison-3.7.5

#Perl-5.32.1

echo Perl-5.32.1 >> err
tar -xf perl-5.32.1.tar.*
cd perl-5.32.1
sh Configure -des                                        \
             -Dprefix=/usr                               \
             -Dvendorprefix=/usr                         \
             -Dprivlib=/usr/lib/perl5/5.32/core_perl     \
             -Darchlib=/usr/lib/perl5/5.32/core_perl     \
             -Dsitelib=/usr/lib/perl5/5.32/site_perl     \
             -Dsitearch=/usr/lib/perl5/5.32/site_perl    \
             -Dvendorlib=/usr/lib/perl5/5.32/vendor_perl \
             -Dvendorarch=/usr/lib/perl5/5.32/vendor_perl
make
make install
cd $LFS/sources
rm -rf perl-5.32.1

#Python-3.9.2

echo Python-3.9.2 >> err
tar -xf Python-3.9.2.tar.*
cd Python-3.9.2
./configure --prefix=/usr   \
            --enable-shared \
            --without-ensurepip
make
make install
cd $LFS/sources
rm -rf Python-3.9.2

#Texinfo-6.7

echo Texinfo-6.7 >> err
tar -xf texinfo-6.7.tar.*
cd texinfo-6.7
./configure --prefix=/usr
make
make install
cd $LFS/sources
rm -rf texinfo-6.7

#Util-linux-2.36.2

echo Util-linux-2.36.2 >> err
tar -xf util-linux-2.36.2.tar.*
cd util-linux-2.36.2
mkdir -pv /var/lib/hwclock
./configure ADJTIME_PATH=/var/lib/hwclock/adjtime    \
            --docdir=/usr/share/doc/util-linux-2.36.2 \
            --disable-chfn-chsh  \
            --disable-login      \
            --disable-nologin    \
            --disable-su         \
            --disable-setpriv    \
            --disable-runuser    \
            --disable-pylibmount \
            --disable-static     \
            --without-python     \
            runstatedir=/run
make
make install
cd $LFS/sources
rm -rf util-linux-2.36.2

find /usr/{lib,libexec} -name \*.la -delete
rm -rf /usr/share/{info,man,doc}/*
