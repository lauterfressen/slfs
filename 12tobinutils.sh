#Zlib-1.2.11

echo Zlib-1.2.11 >> err
tar -xf zlib-1.2.11.tar.*
cd zlib-1.2.11
./configure --prefix=/usr
make

make install
mv -v /usr/lib/libz.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libz.so) /usr/lib/libz.so
rm -fv /usr/lib/libz.a
cd $LFS/sources
rm -rf zlib-1.2.11

#Bzip2-1.0.8

echo Bzip2-1.0.8 >> err
tar -xf bzip2-1.0.8.tar.*
cd bzip2-1.0.8
patch -Np1 -i ../bzip2-1.0.8-install_docs-1.patch
sed -i 's@\(ln -s -f \)$(PREFIX)/bin/@\1@' Makefile
sed -i "s@(PREFIX)/man@(PREFIX)/share/man@g" Makefile
make -f Makefile-libbz2_so
make clean
make
make PREFIX=/usr install
cp -v bzip2-shared /bin/bzip2
cp -av libbz2.so* /lib
ln -sv ../../lib/libbz2.so.1.0 /usr/lib/libbz2.so
rm -v /usr/bin/{bunzip2,bzcat,bzip2}
ln -sv bzip2 /bin/bunzip2
ln -sv bzip2 /bin/bzcat
rm -fv /usr/lib/libbz2.a
cd $LFS/sources
rm -rf bzip2-1.0.8

#Xz-5.2.5

echo Xz-5.2.5 >> err
tar -xf xz-5.2.5.tar.*
cd xz-5.2.5
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/xz-5.2.5
make

make install
mv -v   /usr/bin/{lzma,unlzma,lzcat,xz,unxz,xzcat} /bin
mv -v /usr/lib/liblzma.so.* /lib
ln -svf ../../lib/$(readlink /usr/lib/liblzma.so) /usr/lib/liblzma.so
cd $LFS/sources
rm -rf xz-5.2.5

#Zstd-1.4.8

echo Zstd-1.4.8 >> err
tar -xf zstd-1.4.8.tar.*
cd zstd-1.4.8
make

make prefix=/usr install
rm -v /usr/lib/libzstd.a
mv -v /usr/lib/libzstd.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libzstd.so) /usr/lib/libzstd.so
cd $LFS/sources
rm -rf zstd-1.4.8

#File-5.39

echo File-5.39 >> err
tar -xf file-5.39.tar.*
cd file-5.39
./configure --prefix=/usr
make

make install
cd $LFS/sources
rm -rf file-5.39

#Readline-8.1

echo Readline-8.1 >> err
tar -xf readline-8.1.tar.*
cd readline-8.1
sed -i '/MV.*old/d' Makefile.in
sed -i '/{OLDSUFF}/c:' support/shlib-install
./configure --prefix=/usr    \
            --disable-static \
            --with-curses    \
            --docdir=/usr/share/doc/readline-8.1
make SHLIB_LIBS="-lncursesw"
make SHLIB_LIBS="-lncursesw" install
mv -v /usr/lib/lib{readline,history}.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libreadline.so) /usr/lib/libreadline.so
ln -sfv ../../lib/$(readlink /usr/lib/libhistory.so ) /usr/lib/libhistory.so
install -v -m644 doc/*.{ps,pdf,html,dvi} /usr/share/doc/readline-8.1
cd $LFS/sources
rm -rf readline-8.1

#M4-1.4.18

echo M4-1.4.18 >> err
tar -xf m4-1.4.18.tar.*
cd m4-1.4.18
sed -i 's/IO_ftrylockfile/IO_EOF_SEEN/' lib/*.c
echo "#define _IO_IN_BACKUP 0x100" >> lib/stdio-impl.h
./configure --prefix=/usr
make

make install
cd $LFS/sources
rm -rf m4-1.4.18

#Bc-3.3.0

echo Bc-3.3.0 >> err
tar -xf bc-3.3.0.tar.*
cd bc-3.3.0
PREFIX=/usr CC=gcc ./configure.sh -G -O3
make

make install
cd $LFS/sources
rm -rf bc-3.3.0

#Flex-2.6.4

echo Flex-2.6.4 >> err
tar -xf flex-2.6.4.tar.*
cd flex-2.6.4
./configure --prefix=/usr \
            --docdir=/usr/share/doc/flex-2.6.4 \
            --disable-static
make

make install
ln -sv flex /usr/bin/lex
cd $LFS/sources
rm -rf flex-2.6.4

#Tcl-8.6.11

echo Tcl-8.6.11 >> err
tar -xf tcl8.6.11-src.tar.*
cd tcl8.6.11-src
tar -xf ../tcl8.6.11-html.tar.gz --strip-components=1
SRCDIR=$(pwd)
cd unix
./configure --prefix=/usr           \
            --mandir=/usr/share/man \
            $([ "$(uname -m)" = x86_64 ] && echo --enable-64bit)
make

sed -e "s|$SRCDIR/unix|/usr/lib|" \
    -e "s|$SRCDIR|/usr/include|"  \
    -i tclConfig.sh

sed -e "s|$SRCDIR/unix/pkgs/tdbc1.1.2|/usr/lib/tdbc1.1.2|" \
    -e "s|$SRCDIR/pkgs/tdbc1.1.2/generic|/usr/include|"    \
    -e "s|$SRCDIR/pkgs/tdbc1.1.2/library|/usr/lib/tcl8.6|" \
    -e "s|$SRCDIR/pkgs/tdbc1.1.2|/usr/include|"            \
    -i pkgs/tdbc1.1.2/tdbcConfig.sh

sed -e "s|$SRCDIR/unix/pkgs/itcl4.2.1|/usr/lib/itcl4.2.1|" \
    -e "s|$SRCDIR/pkgs/itcl4.2.1/generic|/usr/include|"    \
    -e "s|$SRCDIR/pkgs/itcl4.2.1|/usr/include|"            \
    -i pkgs/itcl4.2.1/itclConfig.sh

unset SRCDIR

make install
chmod -v u+w /usr/lib/libtcl8.6.so
make install-private-headers
ln -sfv tclsh8.6 /usr/bin/tclsh
mv /usr/share/man/man3/{Thread,Tcl_Thread}.3
cd $LFS/sources
rm -rf tcl8.6.11-src

#Expect-5.45.4

echo Expect-5.45.4 >> err
tar -xf expect5.45.4.tar.*
cd expect5.45.4
./configure --prefix=/usr           \
            --with-tcl=/usr/lib     \
            --enable-shared         \
            --mandir=/usr/share/man \
            --with-tclinclude=/usr/include
make

make install
ln -svf expect5.45.4/libexpect5.45.4.so /usr/lib
cd $LFS/sources
rm -rf expect5.45.4

#DejaGNU-1.6.2

echo DejaGNU-1.6.2 >> err
tar -xf dejagnu-1.6.2.tar.*
cd dejagnu-1.6.2
./configure --prefix=/usr
makeinfo --html --no-split -o doc/dejagnu.html doc/dejagnu.texi
makeinfo --plaintext       -o doc/dejagnu.txt  doc/dejagnu.texi
make install
install -v -dm755  /usr/share/doc/dejagnu-1.6.2
install -v -m644   doc/dejagnu.{html,txt} /usr/share/doc/dejagnu-1.6.2

cd $LFS/sources
rm -rf dejagnu-1.6.2

#Binutils-2.36.1

echo Binutils-2.36.1 >> err
tar -xf binutils-2.36.1.tar.*
cd binutils-2.36.1
expect -c "spawn ls"
sed -i '/@\tincremental_copy/d' gold/testsuite/Makefile.in
mkdir -v build
cd       build
../configure --prefix=/usr       \
             --enable-gold       \
             --enable-ld=default \
             --enable-plugins    \
             --enable-shared     \
             --disable-werror    \
             --enable-64-bit-bfd \
             --with-system-zlib
make tooldir=/usr
make -k check 2>&1 | tee /sources/gmp-check-log
make tooldir=/usr install
rm -fv /usr/lib/lib{bfd,ctf,ctf-nobfd,opcodes}.a
cd $LFS/sources
rm -rf binutils-2.36.1