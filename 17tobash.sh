#Pkg-config-0.29.2

echo Pkg-config-0.29.2 >> err
tar -xf pkg-config-0.29.2.tar.*
cd pkg-config-0.29.2
./configure --prefix=/usr              \
            --with-internal-glib       \
            --disable-host-tool        \
            --docdir=/usr/share/doc/pkg-config-0.29.2
make

make install
cd $LFS/sources
rm -rf pkg-config-0.29.2

#Ncurses-6.2

echo Ncurses-6.2 >> err
tar -xf ncurses-6.2.tar.*
cd ncurses-6.2
./configure --prefix=/usr           \
            --mandir=/usr/share/man \
            --with-shared           \
            --without-debug         \
            --without-normal        \
            --enable-pc-files       \
            --enable-widec
make
make install
mv -v /usr/lib/libncursesw.so.6* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libncursesw.so) /usr/lib/libncursesw.so
for lib in ncurses form panel menu ; do
    rm -vf                    /usr/lib/lib${lib}.so
    echo "INPUT(-l${lib}w)" > /usr/lib/lib${lib}.so
    ln -sfv ${lib}w.pc        /usr/lib/pkgconfig/${lib}.pc
done
rm -vf                     /usr/lib/libcursesw.so
echo "INPUT(-lncursesw)" > /usr/lib/libcursesw.so
ln -sfv libncurses.so      /usr/lib/libcurses.so
rm -fv /usr/lib/libncurses++w.a
mkdir -v       /usr/share/doc/ncurses-6.2
cp -v -R doc/* /usr/share/doc/ncurses-6.2
cd $LFS/sources
rm -rf ncurses-6.2

#Sed-4.8

echo Sed-4.8 >> err
tar -xf sed-4.8.tar.*
cd sed-4.8
./configure --prefix=/usr --bindir=/bin
make
make html

make install
install -d -m755           /usr/share/doc/sed-4.8
install -m644 doc/sed.html /usr/share/doc/sed-4.8
cd $LFS/sources
rm -rf sed-4.8

#Psmisc-23.4

echo Psmisc-23.4 >> err
tar -xf psmisc-23.4.tar.*
cd psmisc-23.4
./configure --prefix=/usr
make
make install
mv -v /usr/bin/fuser   /bin
mv -v /usr/bin/killall /bin
cd $LFS/sources
rm -rf psmisc-23.4

#Gettext-0.21

echo Gettext-0.21 >> err
tar -xf gettext-0.21.tar.*
cd gettext-0.21
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/gettext-0.21
make

make install
chmod -v 0755 /usr/lib/preloadable_libintl.so
cd $LFS/sources
rm -rf gettext-0.21

#Bison-3.7.5

echo Bison-3.7.5 >> err
tar -xf bison-3.7.5.tar.*
cd bison-3.7.5
./configure --prefix=/usr --docdir=/usr/share/doc/bison-3.7.5
make

make install
cd $LFS/sources
rm -rf bison-3.7.5

#Grep-3.6

echo Grep-3.6 >> err
tar -xf grep-3.6.tar.*
cd grep-3.6
./configure --prefix=/usr --bindir=/bin
make

make install
cd $LFS/sources
rm -rf grep-3.6

#Bash-5.1

echo Bash-5.1 >> err
tar -xf bash-5.1.tar.*
cd bash-5.1
sed -i  '/^bashline.o:.*shmbchar.h/a bashline.o: ${DEFDIR}/builtext.h' Makefile.in
./configure --prefix=/usr                    \
            --docdir=/usr/share/doc/bash-5.1 \
            --without-bash-malloc            \
            --with-installed-readline
make

make install
mv -vf /usr/bin/bash /bin