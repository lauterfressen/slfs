#Linux-5.10.17

echo Linux-5.10.17 >> err
tar -xf linux-5.10.17.tar.*
cd linux-5.10.17
make mrproper
make menuconfig
make
make modules_install
cp -iv arch/x86/boot/bzImage /boot/vmlinuz-5.10.17-lfs-10.1
cp -iv System.map /boot/System.map-5.10.17
cp -iv .config /boot/config-5.10.17
install -d /usr/share/doc/linux-5.10.17 
