chroot "$LFS" /usr/bin/env -i          \
    HOME=/root TERM="$TERM"            \
    MAKEFLAGS=´-j7´                    \
    PS1='(lfs chroot) \u:\w\$ '        \
    PATH=/bin:/usr/bin:/sbin:/usr/sbin \
    /bin/bash --login 
