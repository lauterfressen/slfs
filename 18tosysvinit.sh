#Libtool-2.4.6

echo Libtool-2.4.6 >> err
tar -xf libtool-2.4.6.tar.*
cd libtool-2.4.6
./configure --prefix=/usr
make

make install
rm -fv /usr/lib/libltdl.a
cd /sources
rm -rf libtool-2.4.6

#GDBM-1.19

echo GDBM-1.19 >> err
tar -xf gdbm-1.19.tar.*
cd gdbm-1.19
./configure --prefix=/usr    \
            --disable-static \
            --enable-libgdbm-compat
make

make install
cd /sources
rm -rf gdbm-1.19

#Gperf-3.1

echo Gperf-3.1 >> err
tar -xf gperf-3.1.tar.*
cd gperf-3.1
./configure --prefix=/usr --docdir=/usr/share/doc/gperf-3.1
make

make install
cd /sources
rm -rf gperf-3.1

#Expat-2.2.10

echo Expat-2.2.10 >> err
tar -xf expat-2.2.10.tar.*
cd expat-2.2.10
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/expat-2.2.10
make

make install
install -v -m644 doc/*.{html,png,css} /usr/share/doc/expat-2.2.10
cd /sources
rm -rf expat-2.2.10

#Inetutils-2.0

echo Inetutils-2.0 >> err
tar -xf inetutils-2.0.tar.*
cd inetutils-2.0
./configure --prefix=/usr        \
            --localstatedir=/var \
            --disable-logger     \
            --disable-whois      \
            --disable-rcp        \
            --disable-rexec      \
            --disable-rlogin     \
            --disable-rsh        \
            --disable-servers
make

make install
mv -v /usr/bin/{hostname,ping,ping6,traceroute} /bin
mv -v /usr/bin/ifconfig /sbin
cd /sources
rm -rf inetutils-2.0

#Perl-5.32.1

echo Perl-5.32.1 >> err
tar -xf perl-5.32.1.tar.*
cd perl-5.32.1
export BUILD_ZLIB=False
export BUILD_BZIP2=0
sh Configure -des                                         \
             -Dprefix=/usr                                \
             -Dvendorprefix=/usr                          \
             -Dprivlib=/usr/lib/perl5/5.32/core_perl      \
             -Darchlib=/usr/lib/perl5/5.32/core_perl      \
             -Dsitelib=/usr/lib/perl5/5.32/site_perl      \
             -Dsitearch=/usr/lib/perl5/5.32/site_perl     \
             -Dvendorlib=/usr/lib/perl5/5.32/vendor_perl  \
             -Dvendorarch=/usr/lib/perl5/5.32/vendor_perl \
             -Dman1dir=/usr/share/man/man1                \
             -Dman3dir=/usr/share/man/man3                \
             -Dpager="/usr/bin/less -isR"                 \
             -Duseshrplib                                 \
             -Dusethreads

make

make install
unset BUILD_ZLIB BUILD_BZIP2
cd /sources
rm -rf perl-5.32.1

#XML::Parser-2.46

echo XML::Parser-2.46 >> err
tar -xf XML-Parser-2.46.tar.*
cd XML-Parser-2.46
perl Makefile.PL
make

make install
cd /sources
rm -rf XML-Parser-2.46

#Intltool-0.51.0

echo Intltool-0.51.0 >> err
tar -xf intltool-0.51.0.tar.*
cd intltool-0.51.0
sed -i 's:\\\${:\\\$\\{:' intltool-update.in
./configure --prefix=/usr
make

make install
install -v -Dm644 doc/I18N-HOWTO /usr/share/doc/intltool-0.51.0/I18N-HOWTO
cd /sources
rm -rf intltool-0.51.0

#Autoconf-2.71

echo Autoconf-2.71 >> err
tar -xf autoconf-2.71.tar.*
cd autoconf-2.71
./configure --prefix=/usr
make

make install
cd /sources
rm -rf autoconf-2.71

#Automake-1.16.3

echo Automake-1.16.3 >> err
tar -xf automake-1.16.3.tar.*
cd automake-1.16.3
sed -i "s/''/etags/" t/tags-lisp-space.sh
./configure --prefix=/usr --docdir=/usr/share/doc/automake-1.16.3
make

make install
cd /sources
rm -rf automake-1.16.3

#Kmod-28

echo Kmod-28 >> err
tar -xf kmod-28.tar.*
cd kmod-28
./configure --prefix=/usr          \
            --bindir=/bin          \
            --sysconfdir=/etc      \
            --with-rootlibdir=/lib \
            --with-xz              \
            --with-zstd            \
            --with-zlib
make
make install

for target in depmod insmod lsmod modinfo modprobe rmmod; do
  ln -sfv ../bin/kmod /sbin/$target
done

ln -sfv kmod /bin/lsmod
cd /sources
rm -rf kmod-28

#Elfutils-0.183

echo Elfutils-0.183 >> err
tar -xf elfutils-0.183.tar.*
cd elfutils-0.183
./configure --prefix=/usr                \
            --disable-debuginfod         \
            --enable-libdebuginfod=dummy \
            --libdir=/lib
make

make -C libelf install
install -vm644 config/libelf.pc /usr/lib/pkgconfig
rm /lib/libelf.a
cd /sources
rm -rf elfutils-0.183

#Libffi-3.3

echo Libffi-3.3 >> err
tar -xf libffi-3.3.tar.*
cd libffi-3.3
./configure --prefix=/usr --disable-static --with-gcc-arch=native
make

make install
cd /sources
rm -rf libffi-3.3

#OpenSSL-1.1.1j

echo OpenSSL-1.1.1j >> err
tar -xf openssl-1.1.1j.tar.*
cd openssl-1.1.1j
./config --prefix=/usr         \
         --openssldir=/etc/ssl \
         --libdir=lib          \
         shared                \
         zlib-dynamic
make

sed -i '/INSTALL_LIBS/s/libcrypto.a libssl.a//' Makefile
make MANSUFFIX=ssl install
mv -v /usr/share/doc/openssl /usr/share/doc/openssl-1.1.1j
cp -vfr doc/* /usr/share/doc/openssl-1.1.1j
cd /sources
rm -rf openssl-1.1.1j

#Python-3.9.2

echo Python-3.9.2 >> err
tar -xf Python-3.9.2.tar.*
cd Python-3.9.2
./configure --prefix=/usr       \
            --enable-shared     \
            --with-system-expat \
            --with-system-ffi   \
            --with-ensurepip=yes
make

make install
install -v -dm755 /usr/share/doc/python-3.9.2/html 

tar --strip-components=1  \
    --no-same-owner       \
    --no-same-permissions \
    -C /usr/share/doc/python-3.9.2/html \
    -xvf ../python-3.9.2-docs-html.tar.bz2
cd /sources
rm -rf Python-3.9.2

#Ninja-1.10.2

echo Ninja-1.10.2 >> err
tar -xf ninja-1.10.2.tar.*
cd ninja-1.10.2
sed -i '/int Guess/a \
  int   j = 0;\
  char* jobs = getenv( "NINJAJOBS" );\
  if ( jobs != NULL ) j = atoi( jobs );\
  if ( j > 0 ) return j;\
' src/ninja.cc
python3 configure.py --bootstrap

install -vm755 ninja /usr/bin/
install -vDm644 misc/bash-completion /usr/share/bash-completion/completions/ninja
install -vDm644 misc/zsh-completion  /usr/share/zsh/site-functions/_ninja
cd /sources
rm -rf ninja-1.10.2

#Meson-0.57.1

echo Meson-0.57.1 >> err
tar -xf meson-0.57.1.tar.*
cd meson-0.57.1
python3 setup.py build
python3 setup.py install --root=dest
cp -rv dest/* /
cd /sources
rm -rf meson-0.57.1

#Coreutils-8.32

echo Coreutils-8.32 >> err
tar -xf coreutils-8.32.tar.*
cd coreutils-8.32
patch -Np1 -i ../coreutils-8.32-i18n-1.patch
sed -i '/test.lock/s/^/#/' gnulib-tests/gnulib.mk
autoreconf -fiv
FORCE_UNSAFE_CONFIGURE=1 ./configure \
            --prefix=/usr            \
            --enable-no-install-program=kill,uptime
make

make install
mv -v /usr/bin/{cat,chgrp,chmod,chown,cp,date,dd,df,echo} /bin
mv -v /usr/bin/{false,ln,ls,mkdir,mknod,mv,pwd,rm} /bin
mv -v /usr/bin/{rmdir,stty,sync,true,uname} /bin
mv -v /usr/bin/chroot /usr/sbin
mv -v /usr/share/man/man1/chroot.1 /usr/share/man/man8/chroot.8
sed -i 's/"1"/"8"/' /usr/share/man/man8/chroot.8
mv -v /usr/bin/{head,nice,sleep,touch} /bin
cd /sources
rm -rf coreutils-8.32

#Check-0.15.2

echo Check-0.15.2 >> err
tar -xf check-0.15.2.tar.*
cd check-0.15.2
./configure --prefix=/usr --disable-static
make

make docdir=/usr/share/doc/check-0.15.2 install
cd /sources
rm -rf check-0.15.2

#Diffutils-3.7

echo Diffutils-3.7 >> err
tar -xf diffutils-3.7.tar.*
cd diffutils-3.7
./configure --prefix=/usr
make

make install
cd /sources
rm -rf diffutils-3.7

#Gawk-5.1.0

echo Gawk-5.1.0 >> err
tar -xf gawk-5.1.0.tar.*
cd gawk-5.1.0
sed -i 's/extras//' Makefile.in
./configure --prefix=/usr
make

make install

cd /sources
rm -rf gawk-5.1.0

#Findutils-4.8.0

echo Findutils-4.8.0 >> err
tar -xf findutils-4.8.0.tar.*
cd findutils-4.8.0
./configure --prefix=/usr --localstatedir=/var/lib/locate
make

make install
mv -v /usr/bin/find /bin
sed -i 's|find:=${BINDIR}|find:=/bin|' /usr/bin/updatedb
cd /sources
rm -rf findutils-4.8.0

#Groff-1.22.4

echo Groff-1.22.4 >> err
tar -xf groff-1.22.4.tar.*
cd groff-1.22.4
PAGE=<paper_size> ./configure --prefix=/usr
make -j1
make install
cd /sources
rm -rf groff-1.22.4

#GRUB-2.04

echo GRUB-2.04 >> err
tar -xf grub-2.04.tar.*
cd grub-2.04
sed "s/gold-version/& -R .note.gnu.property/" \
    -i Makefile.in grub-core/Makefile.in
./configure --prefix=/usr          \
            --sbindir=/sbin        \
            --sysconfdir=/etc      \
            --disable-efiemu       \
            --disable-werror
make
make install
mv -v /etc/bash_completion.d/grub /usr/share/bash-completion/completions
cd /sources
rm -rf grub-2.04

#Less-563

echo Less-563 >> err
tar -xf less-563.tar.*
cd less-563
./configure --prefix=/usr --sysconfdir=/etc
make
make install
cd /sources
rm -rf less-563

#Gzip-1.10

echo Gzip-1.10 >> err
tar -xf gzip-1.10.tar.*
cd gzip-1.10
./configure --prefix=/usr
make

make install
mv -v /usr/bin/gzip /bin
cd /sources
rm -rf gzip-1.10

#IPRoute2-5.10.0

echo IPRoute2-5.10.0 >> err
tar -xf iproute2-5.10.0.tar.*
cd iproute2-5.10.0
sed -i /ARPD/d Makefile
rm -fv man/man8/arpd.8
sed -i 's/.m_ipt.o//' tc/Makefile
make
make DOCDIR=/usr/share/doc/iproute2-5.10.0 install
cd /sources
rm -rf iproute2-5.10.0

#Kbd-2.4.0

echo Kbd-2.4.0 >> err
tar -xf kbd-2.4.0.tar.*
cd kbd-2.4.0
patch -Np1 -i ../kbd-2.4.0-backspace-1.patch
sed -i '/RESIZECONS_PROGS=/s/yes/no/' configure
sed -i 's/resizecons.8 //' docs/man/man8/Makefile.in
./configure --prefix=/usr --disable-vlock
make

make install

cd /sources
rm -rf kbd-2.4.0

#Libpipeline-1.5.3

echo Libpipeline-1.5.3 >> err
tar -xf libpipeline-1.5.3.tar.*
cd libpipeline-1.5.3
./configure --prefix=/usr
make

make install
cd /sources
rm -rf libpipeline-1.5.3

#Make-4.3

echo Make-4.3 >> err
tar -xf make-4.3.tar.*
cd make-4.3
./configure --prefix=/usr
make

make install
cd /sources
rm -rf make-4.3

#Patch-2.7.6

echo Patch-2.7.6 >> err
tar -xf patch-2.7.6.tar.*
cd patch-2.7.6
./configure --prefix=/usr
make

make install
cd /sources
rm -rf patch-2.7.6

#Man-DB-2.9.4

echo Man-DB-2.9.4 >> err
tar -xf man-db-2.9.4.tar.*
cd man-db-2.9.4
./configure --prefix=/usr                        \
            --docdir=/usr/share/doc/man-db-2.9.4 \
            --sysconfdir=/etc                    \
            --disable-setuid                     \
            --enable-cache-owner=bin             \
            --with-browser=/usr/bin/lynx         \
            --with-vgrind=/usr/bin/vgrind        \
            --with-grap=/usr/bin/grap            \
            --with-systemdtmpfilesdir=           \
            --with-systemdsystemunitdir=
make

make install
cd /sources
rm -rf man-db-2.9.4

#Tar-1.34

echo Tar-1.34 >> err
tar -xf tar-1.34.tar.*
cd tar-1.34
FORCE_UNSAFE_CONFIGURE=1  \
./configure --prefix=/usr \
            --bindir=/bin
make

make install
make -C doc install-html docdir=/usr/share/doc/tar-1.34
cd /sources
rm -rf tar-1.34

#Texinfo-6.7

echo Texinfo-6.7 >> err
tar -xf texinfo-6.7.tar.*
cd texinfo-6.7
./configure --prefix=/usr
make

make install
make TEXMF=/usr/share/texmf install-tex
pushd /usr/share/info
  rm -v dir
  for f in *
    do install-info $f dir 2>/dev/null
  done
popd
cd /sources
rm -rf texinfo-6.7

#Vim-8.2.2433

echo Vim-8.2.2433 >> err
tar -xf vim-8.2.2433.tar.*
cd vim-8.2.2433
echo '#define SYS_VIMRC_FILE "/etc/vimrc"' >> src/feature.h
./configure --prefix=/usr
make

make install
ln -sv vim /usr/bin/vi
for L in  /usr/share/man/{,*/}man1/vim.1; do
    ln -sv vim.1 $(dirname $L)/vi.1
done
ln -sv ../vim/vim82/doc /usr/share/doc/vim-8.2.2433
cd /sources
rm -rf vim-8.2.2433

cat > /etc/vimrc << "EOF"
" Begin /etc/vimrc

" Ensure defaults are set before customizing settings, not after
source $VIMRUNTIME/defaults.vim
let skip_defaults_vim=1 

set nocompatible
set backspace=2
set mouse=
syntax on
if (&term == "xterm") || (&term == "putty")
  set background=dark
endif

" End /etc/vimrc
EOF

#Eudev-3.2.10

echo Eudev-3.2.10 >> err
tar -xf eudev-3.2.10.tar.*
cd eudev-3.2.10
./configure --prefix=/usr           \
            --bindir=/sbin          \
            --sbindir=/sbin         \
            --libdir=/usr/lib       \
            --sysconfdir=/etc       \
            --libexecdir=/lib       \
            --with-rootprefix=      \
            --with-rootlibdir=/lib  \
            --enable-manpages       \
            --disable-static
make
mkdir -pv /lib/udev/rules.d
mkdir -pv /etc/udev/rules.d

make install
tar -xvf ../udev-lfs-20171102.tar.xz
make -f udev-lfs-20171102/Makefile.lfs install
cd /sources
rm -rf eudev-3.2.10

udevadm hwdb --update

#Procps-ng-3.3.17

echo Procps-ng-3.3.17 >> err
tar -xf procps-ng-3.3.17.tar.*
cd procps-ng-3.3.17
./configure --prefix=/usr                            \
            --exec-prefix=                           \
            --libdir=/usr/lib                        \
            --docdir=/usr/share/doc/procps-ng-3.3.17 \
            --disable-static                         \
            --disable-kill
make

make install
mv -v /usr/lib/libprocps.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libprocps.so) /usr/lib/libprocps.so
cd /sources
rm -rf procps-ng-3.3.17

#Util-linux-2.36.2

echo Util-linux-2.36.2 >> err
tar -xf util-linux-2.36.2.tar.*
cd util-linux-2.36.2
./configure ADJTIME_PATH=/var/lib/hwclock/adjtime   \
            --docdir=/usr/share/doc/util-linux-2.36.2 \
            --disable-chfn-chsh  \
            --disable-login      \
            --disable-nologin    \
            --disable-su         \
            --disable-setpriv    \
            --disable-runuser    \
            --disable-pylibmount \
            --disable-static     \
            --without-python     \
            --without-systemd    \
            --without-systemdsystemunitdir \
            runstatedir=/run
make

make install
cd /sources
rm -rf util-linux-2.36.2

#E2fsprogs-1.46.1

echo E2fsprogs-1.46.1 >> err
tar -xf e2fsprogs-1.46.1.tar.*
cd e2fsprogs-1.46.1
mkdir -v build
cd       build
../configure --prefix=/usr           \
             --bindir=/bin           \
             --with-root-prefix=""   \
             --enable-elf-shlibs     \
             --disable-libblkid      \
             --disable-libuuid       \
             --disable-uuidd         \
             --disable-fsck
make

make install
rm -fv /usr/lib/{libcom_err,libe2p,libext2fs,libss}.a
gunzip -v /usr/share/info/libext2fs.info.gz
install-info --dir-file=/usr/share/info/dir /usr/share/info/libext2fs.info
makeinfo -o      doc/com_err.info ../lib/et/com_err.texinfo
install -v -m644 doc/com_err.info /usr/share/info
install-info --dir-file=/usr/share/info/dir /usr/share/info/com_err.info
cd /sources
rm -rf e2fsprogs-1.46.1

#Sysklogd-1.5.1

echo Sysklogd-1.5.1 >> err
tar -xf sysklogd-1.5.1.tar.*
cd sysklogd-1.5.1
sed -i '/Error loading kernel symbols/{n;n;d}' ksym_mod.c
sed -i 's/union wait/int/' syslogd.c
make
make BINDIR=/sbin install
cd /sources
rm -rf sysklogd-1.5.1

cat > /etc/syslog.conf << "EOF"
# Begin /etc/syslog.conf

auth,authpriv.* -/var/log/auth.log
*.*;auth,authpriv.none -/var/log/sys.log
daemon.* -/var/log/daemon.log
kern.* -/var/log/kern.log
mail.* -/var/log/mail.log
user.* -/var/log/user.log
*.emerg *

# End /etc/syslog.conf
EOF

#Sysvinit-2.98

echo Sysvinit-2.98 >> err
tar -xf sysvinit-2.98.tar.*
cd sysvinit-2.98
patch -Np1 -i ../sysvinit-2.98-consolidated-1.patch
make
make install
cd /sources
rm -rf sysvinit-2.98

rm -rf /tmp/* 
