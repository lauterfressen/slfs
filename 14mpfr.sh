#MPFR-4.1.0

echo MPFR-4.1.0 >> err
tar -xf mpfr-4.1.0.tar.*
cd mpfr-4.1.0
./configure --prefix=/usr        \
            --disable-static     \
            --enable-thread-safe \
            --docdir=/usr/share/doc/mpfr-4.1.0
make
make html
make check 2>&1 | tee /sources/gmp-check-log
make install
make install-html
cd $LFS/sources
rm -rf mpfr-4.1.0 
