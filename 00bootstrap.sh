#!/bin/bash

set -uo pipefail
trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR

pacman -Sy --noconfirm pacman-contrib dialog git wget

devicelist=$(lsblk -dplnx size -o name,size | grep -Ev "boot|rpmb|loop" | tac)
device=$(dialog --stdout --menu "Select installtion disk" 0 0 0 ${devicelist}) || exit 1
clear

echo "Updating mirror list"
reflector --verbose --latest 5 --sort rate --save /etc/pacman.d/mirrorlist

### Get infomation from user ###
hostname=$(dialog --stdout --inputbox "Enter hostname" 0 0) || exit 1
clear
: ${hostname:?"hostname cannot be empty"}

user=$(dialog --stdout --inputbox "Enter admin username" 0 0) || exit 1
clear
: ${user:?"user cannot be empty"}

password=$(dialog --stdout --passwordbox "Enter admin password" 0 0) || exit 1
clear
: ${password:?"password cannot be empty"}
password2=$(dialog --stdout --passwordbox "Enter admin password again" 0 0) || exit 1
clear
[[ "$password" == "$password2" ]] || ( echo "Passwords did not match"; exit 1; )

num_root=1

parted $device mklabel msdos mkpart primary ext4 2MiB 25% set 1 boot on

### Set up logging ###
exec 1> >(tee "stdout.log")
exec 2> >(tee "stderr.log")

timedatectl set-ntp true

part_root="$(ls ${device}* | grep -E "^${device}p?${num_root}$")"

wipefs "${part_root}"

mkfs.ext4 "${part_root}"

mount "${part_root}" /mnt

### Install and configure the basic system ###

pacstrap /mnt base linux linux-firmware vim sudo openssh grub git \
              networkmanager samba base-devel gdisk parted dosfstools wget


genfstab -t PARTUUID /mnt >> /mnt/etc/fstab

arch-chroot /mnt ln -sf /usr/share/zoneinfo/Europe/Rome /etc/localtime

arch-chroot /mnt hwclock --systohc

cat << EOF > /mnt/etc/locale.gen
en_US.UTF-8 UTF-8
en_US ISO-8859-1
EOF

arch-chroot /mnt locale-gen

echo "LANG=en_US.UTF-8" > /mnt/etc/locale.conf

echo "${hostname}" > /mnt/etc/hostname

cat << EOF > /mnt/etc/hosts
127.0.0.1	localhost
::1		    localhost
127.0.1.1	$hostname.localdomain	$hostname
EOF

arch-chroot /mnt useradd -m -g wheel "$user"

echo "$user:$password" | chpasswd --root /mnt
echo "root:$password" | chpasswd --root /mnt

arch-chroot /mnt systemctl enable NetworkManager

echo ' %wheel ALL=(ALL) ALL' >> /mnt/etc/sudoers

arch-chroot /mnt grub-install --target=i386-pc $device
arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg